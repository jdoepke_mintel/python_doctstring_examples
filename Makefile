README.md: venv README.md.j2 styles/* render.py
	venv/bin/python render.py README.md.j2 \
		sphinx \
		numpy \
		google \
	> README.md

venv: venv/bin/activate
venv/bin/activate: requirements.txt
	test -d venv || virtualenv venv
	venv/bin/pip install -Uq -r requirements.txt
	touch venv/bin/activate
