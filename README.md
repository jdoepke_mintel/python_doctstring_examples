Python Docstring Examples
=========================
> Examples of several styles of Python docstring.

Table of Contents
=================

  * [sphinx](#sphinx)
  * [numpy](#numpy)
  * [google](#google)


sphinx
======
```python
"""Example sphinx autodoc style docstrings.

.. _Sphinx reST domains
   http://www.sphinx-doc.org/en/stable/domains.html

"""


class ExampleClass(object):
    """The summary line for a class docstring should fit on one line.

    :attr attr1: Description of `attr1`.
    :type attr1: int
    :attr attr2: Description of `attr2`.
    :type attr2: str

    :param param1: The first parameter.
    :type param1: int
    :param param2: The second parameter.
    :type param2: str

    :raises ValueError: If `param2` is equal to `param1`.

    """

    def __init__(self, param1, param2):
        if str(param1) == str(param2):
            raise ValueError('param1 may not be equal to param2')
        self.attr1 = param1
        self.attr2 = param2

    def example_method(self, param1, param2=None, *args, **kwargs):
        """This is an example of a method.

        This method does stuff.

        :param param1: The first parameter.
        :type param1: int
        :param param2: The second parameter.
        :type param2: str, optional
        :param *args: Variable length argument list.
        :param **kwargs: Arbitrary keyword arguments.

        :returns: True if successful, else False.
        :rtype: bool

        """
        return True
```


numpy
=====
```python
"""Example NumPy style docstrings.

.. _NumPy Documentation HOWTO:
   https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt

.. _Complete Example
   http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html

"""


class ExampleClass(object):
    """The summary line for a class docstring should fit on one line.

    Attributes
    ----------
    attr1 : int
        Description of `attr1`.
    attr2 : str
        Description of `attr2`.

    Parameters
    ----------
    param1 : int
        The first parameter.
    param2 : str
        The second parameter.

    Raises
    ------
    ValueError
        If `param2` is equal to `param1`.

    """

    def __init__(self, param1, param2):
        if str(param1) == str(param2):
            raise ValueError('param1 may not be equal to param2')
        self.attr1 = param1
        self.attr2 = param2

    def example_method(self, param1, param2=None, *args, **kwargs):
        """This is an example of a method.

        This method does stuff.

        Parameters
        ----------
        param1 : int
            The first parameter.
        param2 : str, optional
            The second parameter.
        *args
            Variable length argument list.
        **kwargs
            Arbitrary keyword arguments.

        Returns
        -------
         bool
            True if successful, else False.

        """
        return True
```


google
======
```python
"""Example Google style docstrings.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

.. _Complete Example
   http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html

"""


class ExampleClass(object):
    """The summary line for a class docstring should fit on one line.

    Attributes:
        attr1 (int): Description of `attr1`.
        attr2 (str): Description of `attr2`.

    Args:
        param1 (int): The first parameter.
        param2 (str): The second parameter.

    Raises:
        ValueError: If `param2` is equal to `param1`.

    """

    def __init__(self, param1, param2):
        if str(param1) == str(param2):
            raise ValueError('param1 may not be equal to param2')
        self.attr1 = param1
        self.attr2 = param2

    def example_method(self, param1, param2=None, *args, **kwargs):
        """This is an example of a method.

        This method does stuff.

        Args:
            param1 (int): The first parameter.
            param2 (str, optional): The second parameter.
            *args: Variable length argument list.
            **kwargs: Arbitrary keyword arguments.

        Returns:
            bool: True if successful, else False.

        """
        return True
```


