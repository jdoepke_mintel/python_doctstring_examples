"""Example NumPy style docstrings.

.. _NumPy Documentation HOWTO:
   https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt

.. _Complete Example
   http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html

"""


class ExampleClass(object):
    """The summary line for a class docstring should fit on one line.

    Attributes
    ----------
    attr1 : int
        Description of `attr1`.
    attr2 : str
        Description of `attr2`.

    Parameters
    ----------
    param1 : int
        The first parameter.
    param2 : str
        The second parameter.

    Raises
    ------
    ValueError
        If `param2` is equal to `param1`.

    """

    def __init__(self, param1, param2):
        if str(param1) == str(param2):
            raise ValueError('param1 may not be equal to param2')
        self.attr1 = param1
        self.attr2 = param2

    def example_method(self, param1, param2=None, *args, **kwargs):
        """This is an example of a method.

        This method does stuff.

        Parameters
        ----------
        param1 : int
            The first parameter.
        param2 : str, optional
            The second parameter.
        *args
            Variable length argument list.
        **kwargs
            Arbitrary keyword arguments.

        Returns
        -------
         bool
            True if successful, else False.

        """
        return True
