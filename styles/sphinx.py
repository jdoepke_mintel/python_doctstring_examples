"""Example sphinx autodoc style docstrings.

.. _Sphinx reST domains
   http://www.sphinx-doc.org/en/stable/domains.html

"""


class ExampleClass(object):
    """The summary line for a class docstring should fit on one line.

    :attr attr1: Description of `attr1`.
    :type attr1: int
    :attr attr2: Description of `attr2`.
    :type attr2: str

    :param param1: The first parameter.
    :type param1: int
    :param param2: The second parameter.
    :type param2: str

    :raises ValueError: If `param2` is equal to `param1`.

    """

    def __init__(self, param1, param2):
        if str(param1) == str(param2):
            raise ValueError('param1 may not be equal to param2')
        self.attr1 = param1
        self.attr2 = param2

    def example_method(self, param1, param2=None, *args, **kwargs):
        """This is an example of a method.

        This method does stuff.

        :param param1: The first parameter.
        :type param1: int
        :param param2: The second parameter.
        :type param2: str, optional
        :param *args: Variable length argument list.
        :param **kwargs: Arbitrary keyword arguments.

        :returns: True if successful, else False.
        :rtype: bool

        """
        return True
