from __future__ import print_function

import os

import click
import jinja2
from jinja2.ext import Extension
from slugify import slugify


def slug(value):
    return slugify(value, only_ascii=True)


class SlugExtension(Extension):
    def __init__(self, environment):
        super(SlugExtension, self).__init__(environment)
        environment.filters['slug'] = slug


@click.command()
@click.argument('template', nargs=1)
@click.argument('styles', nargs=-1)
def main(template, styles):
    d, filename = os.path.split(template)
    loader = jinja2.FileSystemLoader(d or './')
    env = jinja2.Environment(loader=loader, extensions=(SlugExtension,))
    context = {'styles': styles}
    template = env.get_template(template)
    result = template.render(context)
    print(result)


if __name__ == "__main__":
    main()
